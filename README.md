# Projeto1

Disponibilização de serviços REST para consultas de endereços via CEP

## Estratégia
Desenvolver um projeto leve e altamente testável utilizando Java 8 e ferramentas fortemente consolidadas no mercado de desenvolvimento de *software* corporativo.

## Arquitetura

Visando atender a estratégia estabelecida, o projeto foi desenvolvido utilizando as tecnologias:

* Java 8
* Maven
* Spring Boot
* JUnit
* AngularJS
* REST Apis
* Jetty

## Padrões

Foram adotados alguns padrões e boas práticas no desenvolvimento do Projeto1, visando facilitar o entendimento e melhorar o design do código fonte:

* MVC - Model View Controller
* Injeção de dependência 
* Inversão de controle
* Objetos imutáveis
* REST APIs
* TDD - Test Driven Development

## API
O serviço de consulta de endereço via CEP esta disponível no endereço:

```
#!java

/projeto1/api/cep
```

Segue abaixo um exemplo de uma consulta válida ao serviço:

```
#!java

POST: http://localhost:8080/projeto1/api/cep
{"numero":"01504001"}
```

Resposta:


```
#!java

{
    "rua": "Rua Vergueiro - até 1289 - lado ímpar",
    "bairro": "Liberdade",
    "cidade": "São Paulo",
    "estado": "SP",
    "cep": "01504001"
}
```

Segue um exemplo de uma consulta inválida:

```
#!java

POST: http://localhost:8080/projeto1/api/cep
{"numero":"CEP12300000"}

```

Resposta:


```
#!java

{
    "timestamp": 1446343624362,
    "status": 422,
    "error": "Unprocessable Entity",
    "exception": "br.com.projeto1.exceptions.CepInvalido",
    "message": "CEP inválido",
    "path": "/projeto1/api/cep"
}
```

Segue um exemplo de CEP válido que não possui endereço correspondente, o serviço zera os números, da direita para esquerda, até encontrar um endereço valido para resposta:

```
#!java

POST: http://localhost:8080/projeto1/api/cep
{"numero":"01504999"}

```

Resposta:


```
#!java

{
    "rua": "Rua Vergueiro - até 1288 - lado par",
    "bairro": "Liberdade",
    "cidade": "São Paulo",
    "estado": "SP",
    "cep": "01504000"
}
```

## Interface

Foi desenvolvida uma interface simulando o cadastro de uma nova conta de usuário, o formulário foi desenvolvido utilizando HTML e AngularJS, a interface esta disponível no endereço:

```
#!java

/projeto1/nova-conta.html
```