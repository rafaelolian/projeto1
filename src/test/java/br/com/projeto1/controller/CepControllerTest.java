package br.com.projeto1.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.nio.charset.Charset;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.projeto1.Application;
import br.com.projeto1.domain.Cep;
import br.com.projeto1.service.CepServiceMock;

import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class CepControllerTest {
	
	private MockMvc mockMvc;
	
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));	
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	@Test
    public void deveRetornarEnderecoParaCepExistente() throws Exception {
        mockMvc.perform(post("/api/cep")
                .content( getJson(new Cep(CepServiceMock.CEP_EXISTENTE)) )
                .contentType(contentType))
        		.andExpect(status().isOk())
        		.andExpect(jsonPath("$.cep", is(CepServiceMock.CEP_EXISTENTE)));
    }

	@Test
    public void deveRetornarErroParaCepInvalido() throws Exception {
        mockMvc.perform(post("/api/cep")
                .content( getJson(new Cep("123")) )
                .contentType(contentType))
        		.andExpect( status().is4xxClientError() );
    }

	@Test
    public void deveRetornarVazioParaCepValidoNaoExistente() throws Exception {
        mockMvc.perform(post("/api/cep")
                .content( getJson(new Cep("00000-000")) )
                .contentType(contentType))
        		.andExpect( status().isOk() )
        		.andExpect( content().string("") );
    }

	private String getJson( Object object ) throws JsonProcessingException{
		return new ObjectMapper().writeValueAsString(object); 
	}
}
