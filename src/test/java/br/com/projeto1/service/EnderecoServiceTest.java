package br.com.projeto1.service;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

import br.com.projeto1.domain.Cep;
import br.com.projeto1.domain.Endereco;
import br.com.projeto1.exceptions.CepInvalido;
import br.com.projeto1.service.CepServiceMock;
import br.com.projeto1.service.EnderecoService;

public class EnderecoServiceTest {
	
	private final EnderecoService enderecoService = new EnderecoService(new CepServiceMock());
	
	@Test(expected=CepInvalido.class)
	public void deveRetornarExcecaoParaCepInvalido(){
		enderecoService.buscarEnderecoPorCep( new Cep("1122X2333") );
	}

	@Test
	public void deveRetornarEnderecoParaCepValidoExistente(){
		final Optional<Endereco> endereco = enderecoService.buscarEnderecoPorCep( new Cep(CepServiceMock.CEP_EXISTENTE) );
		Assert.assertTrue(endereco.isPresent());
	}
	
	@Test
	public void naoDeveRetornarEnderecoParaCepValidoInexistente(){
		final Optional<Endereco> endereco = enderecoService.buscarEnderecoPorCep( new Cep("00000-000") );
		Assert.assertFalse(endereco.isPresent());
	}

	@Test
	public void deveRetornarEnderecoParaCepValidoCompletadoComZeroADireita(){
		final Optional<Endereco> endereco = enderecoService.buscarEnderecoPorCep( new Cep("01504999") );
		Assert.assertTrue(endereco.isPresent());
		Assert.assertEquals(CepServiceMock.CEP_ZERADO, endereco.get().getCep());
	}
}