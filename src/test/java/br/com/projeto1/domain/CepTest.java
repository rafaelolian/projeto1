package br.com.projeto1.domain;

import org.junit.Assert;
import org.junit.Test;

import br.com.projeto1.domain.Cep;

public class CepTest 
{
	@Test
	public void deveAceitarCepValidoSemTraco(){
		Cep cep = new Cep("22333999");
		Assert.assertTrue( cep.isValido() );
	}

	@Test
	public void deveAceitarCepValidoComTraco(){
		Cep cep = new Cep("22333-999");
		Assert.assertTrue( cep.isValido() );
	}

	@Test
	public void deveAceitarCepValidoSemTracoComEspaco(){
		Cep cep = new Cep("22333999 ");
		Assert.assertTrue( cep.isValido() );
	}

	@Test
	public void deveAceitarCepValidoComTracoComEspaco(){
		Cep cep = new Cep("22333-999 ");
		Assert.assertTrue( cep.isValido() );
	}

	@Test
	public void deveRejeitarCepNulo(){
		Cep cep = new Cep(null);
		Assert.assertFalse( cep.isValido() );
	}

	@Test
	public void deveRejeitarCepInvalidoComTraco(){
		Cep cep = new Cep("2233-3999");
		Assert.assertFalse( cep.isValido() );
	}

	@Test
	public void deveRejeitarCepMaiorQueLimite(){
		Cep cep = new Cep("223339990");
		Assert.assertFalse( cep.isValido() );
	}

	@Test
	public void deveRejeitarCepMenorQueLimite(){
		Cep cep = new Cep("2233399");
		Assert.assertFalse( cep.isValido() );
	}

	@Test
	public void deveRejeitarCepComLetra(){
		Cep cep = new Cep("2233399A");
		Assert.assertFalse( cep.isValido() );
	}
	
	@Test
	public void deveAdicionarZeroADireitaDoNumero(){
		Cep cep = new Cep("22333999");
		cep.incluirZeroADireita();
		Assert.assertEquals("22333990", cep.getNumero());
		cep.incluirZeroADireita();
		Assert.assertEquals("22333900", cep.getNumero());
		cep.incluirZeroADireita();
		Assert.assertEquals("22333000", cep.getNumero());		
	}

}
