package br.com.projeto1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.projeto1.domain.Cep;
import br.com.projeto1.domain.Endereco;
import br.com.projeto1.service.EnderecoService;

@RestController
@RequestMapping("/api/cep")
public class CepController {
	
	private final EnderecoService enderecoService;
	
	@Autowired
	public CepController( final EnderecoService enderecoService ){
		this.enderecoService = enderecoService;
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public Endereco buscarEnderecoPorCep( @RequestBody final Cep cep ){
		return enderecoService.buscarEnderecoPorCep(cep).orElse(null);
	}
}