package br.com.projeto1.domain;

public class Endereco 
{
	private final String rua;
	private final String bairro;
	private final String cidade;
	private final String estado;
	private final String cep;

	public Endereco(final String rua, final String bairro, final String cidade, final String estado, final String cep) {
		this.rua = rua;
		this.bairro = bairro;
		this.cidade = cidade;
		this.estado = estado;
		this.cep = cep;
	}

	public String getRua() {
		return rua;
	}

	public String getBairro() {
		return bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public String getEstado() {
		return estado;
	}

	public String getCep() {
		return cep;
	}
}
