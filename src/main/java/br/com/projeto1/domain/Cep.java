package br.com.projeto1.domain;

public class Cep 
{
	private String numero;
	
	private Integer contadorZerosADireita=7;

	private static final String CEP_REGEX = "\\d{5}-?\\d{3}";
	
	public Cep(String numero) {
		this.numero = numero;
	}

	public Cep() {
	}

	public boolean isValido(){
		return numero != null && numero.trim().matches(CEP_REGEX);
	}

	public String getNumero() {
		return numero.replaceAll("\\D+","");
	}

	public boolean incluirZeroADireita() {
		Boolean result = false;
		if( contadorZerosADireita >= 0 ){
			final StringBuilder numero = new StringBuilder(getNumero());
			numero.setCharAt(contadorZerosADireita,'0');
			contadorZerosADireita--;
			this.numero = numero.toString();
			result = true;
		}
		return result;
	}
}