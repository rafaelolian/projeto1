package br.com.projeto1.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.projeto1.domain.Cep;
import br.com.projeto1.domain.Endereco;
import br.com.projeto1.exceptions.CepInvalido;

@Service
public class EnderecoService {

	private final CepService cepService;
	
	@Autowired
	public EnderecoService( final CepService cepService ){
		this.cepService = cepService;
	}
	
	public Optional<Endereco> buscarEnderecoPorCep(final Cep cep) throws CepInvalido {
		if( cep.isValido() ){
			return buscarEnderecoPorCepValido(cep);
		} else {
			throw new CepInvalido();
		}
	}
	
	protected Optional<Endereco> buscarEnderecoPorCepValido( final Cep cep ){
		Optional<Endereco> endereco = cepService.buscarEndereco(cep.getNumero());
		
		if( ! endereco.isPresent() ){
			if( cep.incluirZeroADireita() ){
				endereco = buscarEnderecoPorCepValido(cep);
			}
		}
		
		return endereco;
	}
}