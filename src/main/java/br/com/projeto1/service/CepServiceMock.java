package br.com.projeto1.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.projeto1.domain.Endereco;

@Service
public class CepServiceMock implements CepService {

	public static final String CEP_EXISTENTE = "01504001";
	public static final String CEP_ZERADO = "01504000";	
	
	private final Map<String, Endereco> ceps = new HashMap<>();
	
	public CepServiceMock(){
		ceps.put(CEP_EXISTENTE, new Endereco("Rua Vergueiro - até 1289 - lado ímpar", "Liberdade", "São Paulo", "SP", CEP_EXISTENTE));
		ceps.put(CEP_ZERADO, new Endereco("Rua Vergueiro - até 1288 - lado par", "Liberdade", "São Paulo", "SP", CEP_ZERADO));
		ceps.put("12244000", new Endereco("Avenida Shishima Hifumi", "Urbanova", "São José dos Campos", "SP", "12244000"));
		ceps.put("12328460", new Endereco("Avenida Vereador Afonso Rosa da Silva", "Jardim Santa Maria", "Jacareí", "SP", "12328460"));		
	}
	
	@Override
	public Optional<Endereco> buscarEndereco(final String cep) {
		final Endereco endereco = ceps.get(cep);
		return Optional.ofNullable(endereco);
	}
}