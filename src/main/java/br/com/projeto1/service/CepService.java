package br.com.projeto1.service;

import java.util.Optional;

import br.com.projeto1.domain.Endereco;

public interface CepService {

	Optional<Endereco> buscarEndereco(String cep);

}