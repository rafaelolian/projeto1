function contaController($scope, $http) {
	
	$scope.consultarEndereco = function() {
		var cep = {"numero": $scope.conta.cep};
		
		$http.post('api/cep', cep)
		.success(function(data, status, headers, config) {
			$scope.conta.rua = data.rua;
			$scope.conta.bairro = data.bairro;
			$scope.conta.cidade = data.cidade;
			$scope.conta.estado = data.estado;
			$scope.mensagem = "";
		})
		.error(function(data, status, headers, config) {
			$scope.mensagem = data.message;
			$scope.conta = {"cep":$scope.conta.cep};
		});	        	
	}
}